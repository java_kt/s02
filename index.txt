Java Collections, Operators, and Control Structures

Java Collections
- Collections are a single unit of objects
- The collection in Java is a framework that provides an architecture to store and manipulate the group of objects. 

Java collections include the following:
- Arrays
- ArrayLists
- HashMaps

These different types of collections are used based on need and they offer different pre-built methods that would help with development

ARRAYS
- In Java, the number of the contents of an array is defined. You cannot manipulate arrays in Java. 

ARRAY LISTS
- In Java, array lists are arrays that can be controlled. You can create, retrieve, update, and delete elements in an array list. 

HASH MAPS
- A hashmap is like an object where its key:value pairs can be assigned when needed. 


CONTROL STRUCTURES
Control structures control our program. 

