package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class discussion {
    public static void main (String[] args) {
        System.out.println("Hello");
        // Array
        // Uses pre-defined number of elements
            // Declaration
            int[] intArray = new int[5];
            // the [] indicate that this int will hold multiple data
        // "new" is a keyword used to create new variables in non-primitive data types
        // The process made is called instantiation
        // the value inside ([5]) is the number of integers you can put in the array
        // the initial value is zero
        System.out.println(java.util.Arrays.toString(intArray));

        // OR
        for(int i=0; i<intArray.length; i++)
            System.out.println(intArray[i]);

        // OR
        for(Integer i : intArray) {
            System.out.println(i);
        }

        // Declaration with initialization
        int [] intArray2 = {100, 200, 300, 400, 500};
        System.out.println(java.util.Arrays.toString(intArray2));
        // OR
        for(int i=0; i<intArray2.length; i++)
            System.out.println(intArray2[i]);

        // MULTIDIMENSIONAL ARRAYS
        // A multi-dimensional array is an array inside an array

        String classroom[][] = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom));

        // OR USE FOR EACH
        for (String[] x : classroom)
        {
            for (String y : x)
            {
                System.out.print(y + " ");
            }
            System.out.println();
        }

        // OR
        System.out.println(Arrays.deepToString(classroom).replace("], ", "]\n").replace("[[", "[").replace("]]", "]"));

        // OR
        Arrays.stream(classroom).forEach((i) -> {
            Arrays.stream(i).forEach((j) -> System.out.print(j + " "));
            System.out.println();
        });

        // OR
        System.out.println(Arrays.deepToString(classroom)
                .replace("],","\n").replace(",","\t| ")
                .replaceAll("[\\[\\]]", " "));


        // ARRAYLISTS
        // ArrayLists are resizable arrays
        ArrayList<String> students = new ArrayList<String>();
        students.add("John");
        students.add("Paul");

        students.get(0);

        students.set(1, "George");
        System.out.println(students.size());
        students.remove(1);
        System.out.println(students.size());
        students.clear();

        System.out.println(students.size());

        // HashMaps
        // If arrays are used to store items as an ordered collection and the values are access through indices,
        // HashMaps store values as key-value pairs, where the values are accessed through the keys
        // HashMaps are stored by specifying the data type of the key and the value

        HashMap<String, String> job_position = new HashMap<String, String>();

        // Adding elements to HashMaps us by using the put() function
        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");

        // Get the keys of the elements
        System.out.println(job_position.keySet());

        // Elements of HashMaps can be accessed by using the get() function
        System.out.println(job_position.get("Alice"));
        job_position.get("Alice");

        // Removing elements in hashmap is done by using the remove() function
        job_position.remove("Brandon");
        System.out.println(job_position.keySet());

        // Operators
        // Types of Operators:
        // 1. Arithmetic: "-", "+", "*", "/", "%"
        // 2. Comparison: ">", "<", "<=", ">=", "==", !="
        // 3. Logical: "->", "&&", "||", "!"
        // 4. Assignment: "->", "="

        // Control Structures
        // "If" statements manipulate the flow of the code depending on the evaluation of logic expression. If the condition is true, the code will do it.
/*        if (condition) {
            // this will execute if the condition is true
        }*/

        // "if else" statements proceed to the next block of codes if the prior code is true
/*        if (condition) {
            // do this code if the condition is true
        } else {
            // do this is this is the true one
        }*/

        // Short circuiting
        // It is a technique only to the AND and OR operator
/*        if (y > 0 || x / y == 0) {
            System.out.println("Result is: " + x/y);
        }*/

        // Switch cases
        // Switch statements are control flow structures that allow one code block out of many other code blocks.
        // Switch statements are particularly useful when dealing with small discrete (finite) sets of values similar to the following: Direction, Days of the week, Months
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter direction:");
        String directionValue = scan.nextLine();


        switch(directionValue){
            case "1": //A case block within a switch statement. This represents a single case, or a single possible value for the statement
                System.out.println("North");
                break; //The break keyword tells that this specific case block has finished.
//If there is no proper break statement in a case, the code will bleed over to the next case
            case "2":
                System.out.println("South");
                break;
            case "3":
                System.out.println("East");
                break;
            case "4":
                System.out.println("West");
                break;
            default: //the default block handles the scenario if there are no cases that were satisfied
                System.out.println("Invalid");
        }























    }
}





